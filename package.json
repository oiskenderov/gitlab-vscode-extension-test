{
  "name": "gitlab-workflow",
  "displayName": "GitLab Workflow",
  "description": "Official GitLab-maintained extension for Visual Studio Code.",
  "version": "3.81.0",
  "publisher": "GitLab",
  "license": "MIT",
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension"
  },
  "engines": {
    "vscode": "^1.68.0"
  },
  "categories": [
    "Other"
  ],
  "keywords": [
    "git",
    "gitlab",
    "merge request",
    "pipeline",
    "ci cd"
  ],
  "activationEvents": [
    "onStartupFinished"
  ],
  "bugs": {
    "url": "https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues",
    "email": "incoming+gitlab-org-gitlab-vscode-extension-5261717-issue-@incoming.gitlab.com"
  },
  "galleryBanner": {
    "color": "#171321",
    "theme": "dark"
  },
  "contributes": {
    "viewsContainers": {
      "activitybar": [
        {
          "id": "gitlab-duo",
          "title": "GitLab Duo Chat",
          "icon": "assets/images/light/gitlab-duo.svg"
        }
      ]
    },
    "views": {
      "gitlab-duo": [
        {
          "type": "webview",
          "id": "gl.chatView",
          "name": "Experiment",
          "when": "gitlab.featureFlags.chat && gitlab:validState"
        }
      ]
    },
    "commands": [
      {
        "command": "gl.showOutput",
        "title": "Show Extension Logs",
        "category": "GitLab"
      },
      {
        "command": "gl.openChat",
        "title": "Open chat",
        "category": "GitLab Duo Chat"
      },
      {
        "command": "gl.explainSelectedCode",
        "title": "Explain selected code",
        "category": "GitLab Duo Chat"
      },
      {
        "command": "gl.generateTests",
        "title": "Generate Tests",
        "category": "GitLab Duo"
      },
      {
        "command": "gl.newChatConversation",
        "title": "Start a new conversation",
        "category": "GitLab Duo Chat"
      },
      {
        "command": "gl.toggleCodeSuggestions",
        "title": "Toggle Gitlab code suggestions",
        "category": "Gitlab"
      }
    ],
    "submenus": [
      {
        "id": "gl.gitlabDuo",
        "label": "GitLab Duo Chat"
      }
    ],
    "menus": {
      "commandPalette": [
        {
          "command": "gl.openChat",
          "when": "gitlab.featureFlags.chat && gitlab:validState"
        },
        {
          "command": "gl.explainSelectedCode",
          "when": "gitlab.featureFlags.chat && gitlab:validState && editorHasSelection"
        },
        {
          "command": "gl.generateTests",
          "when": "gitlab.featureFlags.chat && gitlab:validState && activeEditor"
        },
        {
          "command": "gl.newChatConversation",
          "when": "gitlab.featureFlags.chat && gitlab:validState"
        }
      ],
      "editor/context": [
        {
          "group": "z_commands",
          "submenu": "gl.gitlabDuo"
        }
      ],
      "gl.gitlabDuo": [
        {
          "command": "gl.explainSelectedCode",
          "group": "navigation",
          "when": "gitlab.featureFlags.chat && gitlab:validState && editorHasSelection"
        },
        {
          "command": "gl.generateTests",
          "group": "navigation",
          "when": "gitlab.featureFlags.chat && gitlab:validState && activeEditor"
        }
      ]
    },
    "keybindings": [
      {
        "command": "gl.openChat",
        "when": "gitlab.featureFlags.chat && gitlab:validState",
        "key": "alt+d"
      },
      {
        "command": "gl.explainSelectedCode",
        "when": "gitlab.featureFlags.chat && gitlab:validState && editorHasSelection",
        "key": "alt+e"
      },
      {
        "command": "gl.generateTests",
        "when": "gitlab.featureFlags.chat && gitlab:validState && activeEditor",
        "key": "alt+t"
      },
      {
        "command": "gl.newChatConversation",
        "when": "gitlab.featureFlags.chat && gitlab:validState",
        "key": "alt+n"
      }
    ],
    "configuration": {
      "title": "GitLab Workflow (GitLab VSCode Extension)",
      "properties": {
        "gitlab.debug": {
          "type": "boolean",
          "default": false,
          "description": "Turning on debug mode turns on better stack trace resolution (source maps) and shows more detailed logs. Restart the extension after enabling this option."
        },
        "gitlab.aiAssistedCodeSuggestions.enabled": {
          "description": "Enable code completion (Beta)",
          "type": "boolean",
          "default": true
        },
        "gitlab.aiAssistedCodeSuggestions.preferredAccount": {
          "description": "GitLab account to use for code completion",
          "type": "string",
          "default": null
        }
      }
    },
    "icons": {
      "gitlab-code-suggestions-loading": {
        "description": "GitLab Code Suggestions Loading",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA01"
        }
      },
      "gitlab-code-suggestions-enabled": {
        "description": "GitLab Code Suggestions Enabled",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA02"
        }
      },
      "gitlab-code-suggestions-disabled": {
        "description": "GitLab Code Suggestions Disabled",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA03"
        }
      },
      "gitlab-code-suggestions-error": {
        "description": "GitLab Code Suggestions Error",
        "default": {
          "fontPath": "./assets/gitlab_icons.woff",
          "fontCharacter": "\\eA04"
        }
      }
    }
  },
  "scripts": {
    "watch:desktop": "node scripts/watch_desktop.mjs",
    "build:desktop": "node scripts/build_desktop.mjs",
    "build:browser": "node scripts/build_browser.mjs",
    "live:browser": "npm run build:browser && vscode-test-web --browserType=chromium --extensionDevelopmentPath=./dist-browser",
    "package": "node scripts/package.mjs",
    "publish": "vsce publish",
    "clean": "node scripts/clean.mjs",
    "lint": "eslint --ext .js --ext .ts --ext .mjs . && prettier --check '**/*.{js,ts,mjs,vue,json,md}' && npm run --prefix webviews/vue lint && npm run --prefix webviews/vue2 lint",
    "test": "npm run test:unit && npm run test:integration",
    "test:unit": "jest",
    "prepare:test:integration": "npm run build:desktop && cp -R node_modules dist-desktop/ && node scripts/create_test_workspace.mjs",
    "test:integration": "npm run prepare:test:integration && node ./dist-desktop/test/runTest.js",
    "prettier-package-json": "prettier --write package.json",
    "prettier": "prettier --write '**/*.{js,ts,mjs,vue,json,md}'",
    "autofix": "npm run clean && eslint --fix . && npm run prettier && cd webviews/vue && npm run autofix",
    "update-ci-variables": "node ./scripts/update_ci_variables.js",
    "create-test-workspace": "npm run build:desktop && node ./scripts/create_workspace_for_test_debugging.js",
    "version": "conventional-changelog -p angular -i CHANGELOG.md -s && git add CHANGELOG.md",
    "postinstall": "npm install --prefix webviews/vue && npm install --prefix webviews/vue2 && npm run prettier-package-json"
  },
  "devDependencies": {
    "@jest/globals": "^29.7.0",
    "@types/jest": "^29.5.6",
    "@types/lodash": "^4.14.200",
    "@types/node": "^13.13.52",
    "@types/request-promise": "^4.1.50",
    "@types/semver": "^7.5.4",
    "@types/sinon": "^10.0.20",
    "@types/source-map-support": "^0.5.9",
    "@types/temp": "^0.9.2",
    "@types/uuid": "^9.0.6",
    "@types/vscode": "^1.68.0",
    "@typescript-eslint/eslint-plugin": "^6.9.0",
    "@typescript-eslint/parser": "^6.9.0",
    "@vscode/test-web": "^0.0.47",
    "conventional-changelog-cli": "^4.1.0",
    "esbuild": "^0.19.5",
    "eslint": "^8.52.0",
    "eslint-config-airbnb-base": "^15.0.0",
    "eslint-config-prettier": "^9.0.0",
    "eslint-plugin-import": "^2.29.0",
    "execa": "^8.0.1",
    "fs-extra": "^11.1.1",
    "jest": "^29.7.0",
    "jest-junit": "^16.0.0",
    "jest-mock-vscode": "^2.1.0",
    "mocha": "^10.2.0",
    "mocha-junit-reporter": "^2.2.1",
    "msw": "^1.3.2",
    "prettier": "^3.0.3",
    "simple-git": "^3.20.0",
    "sinon": "^17.0.0",
    "ts-jest": "^29.1.1",
    "typescript": "^5.2.2",
    "vsce": "^2.15.0",
    "vscode-test": "^1.6.1",
    "webfont": "^11.2.26"
  },
  "dependencies": {
    "@gitlab-org/gitlab-lsp": "^3.7.0",
    "@snowplow/tracker-core": "3.16.0",
    "cross-fetch": "^3.1.8",
    "dayjs": "^1.11.10",
    "graphql": "^16.8.1",
    "graphql-request": "^6.1.0",
    "https-proxy-agent": "^5.0.1",
    "lodash": "^4.17.21",
    "semver": "^7.5.4",
    "source-map-support": "^0.5.21",
    "temp": "^0.9.4",
    "uuid": "^9.0.1",
    "vscode-languageclient": "^9.0.1"
  }
}
