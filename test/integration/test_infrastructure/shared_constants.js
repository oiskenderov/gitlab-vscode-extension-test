module.exports.REMOTE = {
  NAME: 'origin',
  URL: 'git@test.gitlab.com:gitlab-org/gitlab.git',
};
module.exports.DEFAULT_VS_CODE_SETTINGS = {
  'files.enableTrash': false,
  'gitlab.debug': true,
};
